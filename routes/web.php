<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
*/
Auth::routes();

Route::middleware('auth')->group(function(){
    //teste de grupo de rota
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/', 'HomeController@index')->name('home');

    Route::get('/events', 'EventController@eventList');
    Route::get('/alerts', 'AlertController@alertList')->name('alerts');

    Route::post('/alerts', 'AlertController@filterAlert')->name('filterAlert');

    //Rota não utilizada
    //Route::get('/alertsFill', 'alertController@fillAlert')->name('/alertsFill');

    //Rotas da tela de listagem de alertas:
        //Edição de alerta
    Route::get('/empresa/alerts/edicao', 'alertController@editAlert')->name('alert.edit');
        //Criação de alerta
    Route::get('/empresa/alerts/criacao', 'alertController@createAlert')->name('alert.create');

    Route::post('/alertsCad/insert', 'alertController@insertAlert')->name('alertsCad.insert');
    //Route::post('/alertsCad/update', 'alertController@updateAlert')->name('alertsCad.update');
    Route::post('/alertsCad/update/{id}', 'alertController@updateAlert')->name('alertsCad.update');
    Route::get('/alertsCad/delete', 'AlertController@deleteAlert')->name('alertsCad.delete');
});

/*
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index')->name('home');

Route::get('/events', 'EventController@eventList');
Route::get('/alerts', 'AlertController@alertList');

*/

//Route::get('/', 'EventController@events');
