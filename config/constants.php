<?php

    return [
        //Inserir constantes aqui
        'alertAtivo_ON' => '1',
        'alertAtivo_OFF' => '0',


        //Constantes De status
        'Status_insert_success' => '0',
        'Status_insert_fail' => '1',
        'Status_delete_success' => '2',
        'Status_delete_fail' => '3',
        'Status_update_success' => '4', 
        'Status_update_fail' => '5',
        'Status_list_fail' => '6',

        //Constantes de Mensagens de Status
        'Status_insert_success_msg' => 'Alerta Criado com sucesso',
        'Status_insert_fail_msg' => 'Houve algum problema ao criar o alerta.',
        'Status_delete_success_msg' => 'Alerta Excluido com sucesso',
        'Status_delete_fail_msg' => 'Houve algum problema ao excluir o alerta',
        'Status_update_success_msg' => 'Alerta Editado com sucesso', 
        'Status_update_fail_msg' => 'Houve algum problema ao editar o alerta',
        'Status_list_fail_msg' => "Algo inesperado aconteceu! ",

        //Constantes de filtro
        'typeFilter_Ativos' => '1',
        'typeFilter_Inativos' => '2',
        'typeFilter_Expirados' => '3',
        'typeFilter_Periodo' => '4',
    ];

?>