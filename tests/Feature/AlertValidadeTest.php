<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Models\Empresa\User;

class AlertValidadeTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

     //Como chamar o phpUnit
        // phpunit ../../tests/Feature/alertValidadeTest
        
    //Print de mensagens no console durante o teste:
        //fwrite(STDERR, print_r($Myvar, TRUE));


    public function testHasCreationRoute(){
        //$responseByUrl = $this->get(route('/alerts'));
        $response = $this->get(route('alert.create'));
        //$response->assertViewIs('\empresa\alerts\criacao');

        $response->assertStatus(302);

        //$responseByUrl->assertSuccessful(); 
        
    }

    public function testHasCreationWithValidate(){
        //Cria um usuário para fazer a criação
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->get(route('alerts.create'));
        //$response->assertSucessful();
    }
}
