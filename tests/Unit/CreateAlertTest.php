<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateAlertTest extends TestCase
{
   //use App\Models\Empresa\User;
   use User;
   //use DatabaseMigrations;
   /**
    * A basic feature test example.
    *
    * @return void
    */

   //Como chamar o phpUnit
    // phpunit ../../tests/Feature/alertTest

   public function testalertCanBeCreated() {
       //Cria um usuário para fazer o teste

       $user = factory(App\Models\Empresa\User::class)->create();

       $alert = $user->alert()->create([
           'alertName'  => 'Criado por teste',
           'alertDesc'  => 'Descrição criada por teste',
           'alertData'  => '2019-01-01',
           'alertTime'  => '11:45:00',
           'alertAtivo' => '1',
           'idEvt'      => '2',
       ]);

       $this->assertEquals($alert->alertName, 'Criado por teste');
       $this->assertNull($alert);
       
   }

}
