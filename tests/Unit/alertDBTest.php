<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Empresa\User;
use App\Models\Empresa\Alert;

class alertDBTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
   
    //Como chamar o phpUnit
        // phpunit ../../tests/Unit/alertDBTest
        
    //Print de mensagens no console durante o teste:
        //fwrite(STDERR, print_r($Myvar, TRUE));


    public function testalertCanBeCreated() {
    //Teste para verificar se um alerta consegue ser criado
        //Cria um usuário para fazer o teste
        $user = factory(User::class)->create();

        //Cria um alerta, somente pelo model
        $alert = $user->alert()->create([
            'alertName'  => 'Criado por teste',
            'alertDesc'  => 'Descrição criada por teste',
            'alertData'  => '2019-01-01',
            'alertTime'  => '11:45:00',
            'alertAtivo' => '1',
            'idEvt'      => '2',
        ]);


        $this->assertEquals($alert->alertName, 'Criado por teste');
        
    }

    
    public function testalertCanBeDeleted() {
        //Teste para verificar se um alerta consegue ser excluído

        //Cria um usuário e um alerta primeiro

        $user = factory(User::class)->create();

        //Cria um alerta, somente pelo model
            //Não criamos factories, isso vai deixar o processo um pouquinho mais manual
        $alert = $user->alert()->create([
            'alertName'  => 'Criado por teste 3 ',
            'alertDesc'  => 'Descrição',
            'alertData'  => '2019-12-11',
            'alertTime'  => '13:45:00',
            'alertAtivo' => '0',
            'idEvt'      => '3',
        ]);
        //Deletar o alerta criado
        Alert::destroy($alert->id);

        //$alertList = App\Alert::all();

        $this->assertDatabaseMissing('alerts', ['id' => $alert->id]);
        

    }

    public function testalertCanBeListed() {
        //Teste para verificar se o sistema consegue gerar uma lista de alertas

        //Cria um usuário
        $user = factory(User::class)->create();

        //Cria dois alertas
            //Não implantei factories, então ainda estamos no processo manual
        $alert = $user->alert()->create([
            'alertName'  => 'Alerta de teste 1',
            'alertDesc'  => 'bb',
            'alertData'  => '2019-12-11',
            'alertTime'  => '13:45:00',
            'alertAtivo' => '0',
            'idEvt'      => '3',
        ]);
        $alert = $user->alert()->create([
            'alertName'  => 'Alerta de teste 2',
            'alertDesc'  => 'aa',
            'alertData'  => '2019-12-11',
            'alertTime'  => '13:45:00',
            'alertAtivo' => '0',
            'idEvt'      => '3',
        ]);

        $alertList = Alert::all();
        $this->assertTrue(sizeof($alertList)>=2);
    }

    public function testalertCanBeUpdated(){
        //Teste para verificar se o sistema consegue alterar um alerta

        //Cria um Usuário
        $user = factory(User::class)->create();
        //Cria um alerta
        $alert = $user->alert()->create([
            'alertName'  => 'Alarte de teste de edição',
            'alertDesc'  => 'alarte',
            'alertData'  => '2019-12-11',
            'alertTime'  => '13:45:00',
            'alertAtivo' => '0',
            'idEvt'      => '2',
        ]);

        //edita o alerta
        $alertRecovered = Alert::find($alert->id);
        $alertRecovered->alertName = 'Alerta de teste de edição';
        $alertRecovered->alertDesc = 'alerta';

        $alertRecovered->save();
        //verifica se o alerta foi editado
        
        $this->assertEquals(Alert::find($alert->id)->alertDesc, 'alerta');
    }
}
