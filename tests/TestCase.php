<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    // Como chamar o php unit
    // phpunit ../../tests/Unit/CreateAlertTest
}
