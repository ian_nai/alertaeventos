@extends('adminlte::page')

@section('title', 'Eventos do Sistema')

@section('content_header')
    <h1>Eventos do Sistema</h1>
@stop

@section('content')
    



    <div class = "box box-primary">
        <div class = "box-header">
            <!--<a href="" class="btn btn-primary">Novo evento</a>-->
        </div>
        <div class = "box-body">
        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row"><div class="col-sm-6"></div><div class="col-sm-6"></div></div><div class="row"><div class="col-sm-12"><table id="table_alert_user" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="table_alert_user_info">
            <thead>
                <tr role="row">
                    <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Nome do Evento: activate to sort column descending">Ações</th>
                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Descricao do evento: activate to sort column ascending">Status</th>   
                </tr>
            </thead>
            <tbody>
                @foreach($events as $event)
                    <tr role="row" class="odd">
                        <td> {{$event->evtName}} </td>
                        <td> {{$event->evtDesc}} </td>
                    </tr>
                @endforeach

            </tbody>
        <!-- Exemplo de TFoot
            <tfoot>
            <tr><th rowspan="1" colspan="1">Rendering engine</th><th rowspan="1" colspan="1">Browser</th><th rowspan="1" colspan="1">Platform(s)</th><th rowspan="1" colspan="1">Engine version</th><th rowspan="1" colspan="1">CSS grade</th></tr>
            </tfoot>
        -->
            </table></div></div>
        <!-- Pensar em como mostrar os próximos registros depois
            <div class="row"><div class="col-sm-5"><div class="dataTables_info" id="example2_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div></div><div class="col-sm-7"><div class="dataTables_paginate paging_simple_numbers" id="example2_paginate"><ul class="pagination"><li class="paginate_button previous disabled" id="example2_previous"><a href="#" aria-controls="example2" data-dt-idx="0" tabindex="0">Previous</a></li><li class="paginate_button active"><a href="#" aria-controls="example2" data-dt-idx="1" tabindex="0">1</a></li><li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="2" tabindex="0">2</a></li><li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="3" tabindex="0">3</a></li><li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="4" tabindex="0">4</a></li><li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="5" tabindex="0">5</a></li><li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="6" tabindex="0">6</a></li><li class="paginate_button next" id="example2_next"><a href="#" aria-controls="example2" data-dt-idx="7" tabindex="0">Next</a></li></ul></div></div></div></div>
        </div>
        -->
        <!-- /.box-body -->
        </div>
        </div>
    </div>


@stop