@extends('adminlte::page')

@section('title', 'Alertas do Usuário')

@section('content_header')
    <h1>Alertas do usuário</h1>
@stop

@section('content')

        <!-- https://adminlte.io/themes/AdminLTE/pages/forms/general.html -->

        <div class="box box-primary">
            <div class="box-header with-border">
              <!-- <h3 class="box-title">Quick Example</h3> -->
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            
            @if(isset($alert))
            
                <form role="form" method="POST" action="{{route('alertsCad.update')}}" >
                @csrf
                <div class="box-body">
                    <div class="form-group">
                    <input type="hidden" name="id" value="{{$alert->id}}">
                    <label for="status">Status do alerta</label> <br>
                    <input type="radio" name="alertAtivo" id="alertAtivo" value="{{Config::get('constants.alertAtivo_ON')}}"
                    @if($alert->alertAtivo == Config::get('constants.alertAtivo_ON'))
                        checked
                    @endif
                        required > Ativo <br>
                    <input type="radio" name="alertAtivo" id="alertAtivo" value="{{Config::get('constants.alertAtivo_OFF')}}" 
                    @if($alert->alertAtivo == Config::get('constants.alertAtivo_OFF'))
                        checked
                    @endif
                        required> Inativo <br>
                    </div>
                    <div class="form-group">
                    <label for="alertName">Nome do Alerta</label>
                        <input type="text" class="form-control" name = "alertName" id="alertName" value="{{$alert->alertName}}"  required>
                    
                    </div>
                    <div class="form-group">
                    <label for="alertDesc">Descrição do Alerta</label>
                    <textarea class="form-control" rows="2" maxlength="1000" name="alertDesc" cols="50" id="alertDesc" value="{{$alert->alertDesc}}">{{$alert->alertDesc}}</textarea>
                    </div>
                    <div class="form-group">
                    <label for="alertData">Data do Alerta</label>
                    <input type="date" class="form-control" id="alertData" name="alertData" value="{{$alert->alertData}}" required>
                    </div>
                    <div class="form-group">
                    <label for="alertTime">Horário do Alerta</label>
                    <input type="time" class="form-control" id="alertTime" name="alertTime" value="{{$alert->alertTime}}"required>
                    </div>

                    <div class="form-group">
                        <label for="EventAlert">Evento relacionado</label> <br>
                        @foreach($events as $event)
                            <input type="radio" id="{{$event->id}}" name="idEvt" value="{{$event->id}}" 
                            @if($event->id == $alert->idEvt) 
                                checked
                            @endif
                            required>{{$event->evtName}} <br> 
                        @endforeach
                    </div>

                    <!--
                    <div class="form-group">
                    <label for="exampleInputFile">File input</label>
                    <input type="file" id="exampleInputFile">

                    <p class="help-block">Example block-level help text here.</p>
                    </div>
                    <div class="checkbox">
                    <label>
                        <input type="checkbox"> Check me out
                    </label>
                    </div>
                    -->
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Salvar</button>
                </div>
                </form>
            @else
            <form role="form" method="POST" action="{{route('alertsCad.insert')}}" >
                @csrf
                <div class="box-body">
                    <div class="form-group">
                    <label for="status">Status do alerta</label> <br>
                    <input type="radio" name="alertAtivo" id="alertAtivo" value="{{Config::get('constants.alertAtivo_ON')}}" required > Ativo <br>
                    <input type="radio" name="alertAtivo" id="alertAtivo" value="{{Config::get('constants.alertAtivo_OFF')}}" required> Inativo <br>
                    </div>
                    <div class="form-group">
                    <label for="alertName">Nome do Alerta</label>
                        <input type="text" class="form-control" name = "alertName" id="alertName" required >
                    
                    </div>
                    <div class="form-group">
                    <label for="alertDesc">Descrição do Alerta</label>
                    <textarea class="form-control" rows="2" maxlength="1000" name="alertDesc" cols="50" id="alertDesc"></textarea>
                    </div>
                    <div class="form-group">
                    <label for="alertData">Data do Alerta</label>
                    <input type="date" class="form-control" id="alertData" name="alertData" required>
                    </div>
                    <div class="form-group">
                    <label for="alertTime">Horário do Alerta</label>
                    <input type="time" class="form-control" id="alertTime" name="alertTime" required>
                    </div>

                    <div class="form-group">
                        <label for="EventAlert">Evento relacionado</label> <br>
                        @foreach($events as $event)
                            <input type="radio" id="{{$event->id}}" name="idEvt" value="{{$event->id}}" required>{{$event->evtName}} <br> 
                        @endforeach
                    </div>

                    <!--
                    <div class="form-group">
                    <label for="exampleInputFile">File input</label>
                    <input type="file" id="exampleInputFile">

                    <p class="help-block">Example block-level help text here.</p>
                    </div>
                    <div class="checkbox">
                    <label>
                        <input type="checkbox"> Check me out
                    </label>
                    </div>
                    -->
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Salvar</button>
                </div>
                </form>
            @endif
        </div>


@stop