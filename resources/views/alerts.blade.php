@extends('adminlte::page')

@section('title', 'Alertas do Usuário')

@section('content_header')
    <h1>Alertas do usuário</h1>
@stop

@section('content')

    <!--
        Base:
        https://adminlte.io/themes/AdminLTE/pages/tables/data.html
    -->
    @if(isset($status))
        
        @switch($status["code"])
            @case(Config::get('constants.Status_insert_fail'))
            @case(Config::get('constants.Status_update_fail'))
            @case(Config::get('constants.Status_delete_fail'))
                <div class="alert alert-danger alert-dismissible">
                <h4><i class="icon fa fa-exclamation"></i> Status</h4>    
            @break
            @case(Config::get('constants.Status_insert_success'))
            @case(Config::get('constants.Status_update_success'))
            @case(Config::get('constants.Status_delete_success'))
                <div class="alert alert-success alert-dismissible">
                <h4><i class="icon fa fa-check"></i> Status</h4>
            @break
            @case(Config::get('constants.Status_list_fail'))
                <div class="alert alert-warning alert-dismissible">
                <h4><i class="icon fa fa-check"></i> Status</h4>
            @break
        @endswitch
        {{$status["msg"]}} <br>
        {{$status["log"]}}
        </div>
        
    @endif

    <div class="box">
        <div class="box-header">
        <!--  <h3 class="box-title">Hover Data Table</h3> -->
            <a href="{{route('alert.create')}}" class="btn btn-primary">Novo alerta</a>
        </div>
            
        
        <!-- /.box-header -->
        <div class="box-body">

            <form role="form" method="POST" action="{{route('filterAlert')}}" >
                @csrf
                <!-- Divs para Filtros -->
                <div class="row">
                    <label for="typeFilter">Tipo de busca</label>
                </div>

                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="typeFilter" id="typeFilter" value="{{Config::get('constants.typeFilter_Ativos')}}"  > Ativos <br>
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="typeFilter" id="typeFilter" value="{{Config::get('constants.typeFilter_Inativos')}}" > Inativos <br>
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="typeFilter" id="typeFilter" value="{{Config::get('constants.typeFilter_Expirados')}}" > Expirados <br>
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="typeFilter" id="typeFilter" value="{{Config::get('constants.typeFilter_Periodo')}}" > No Período: <br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="row">        
                        <!--<div class="form-group col-sm-4" id="de_div"> -->
                                <label for="de">Data Inicial:</label>
                        </div>
                        <div class="row">
                            <div class="form-group" id="de_div">
                                <input class="form-control" id="de" name="filter_de" type="date">   
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-1">
                    </div>

                    <div class="col-sm-4">
                        <div class="row">        
                        <!--<div class="form-group col-sm-4" id="de_div"> -->
                                <label for="de">Data Final:</label>
                        </div>
                        <div class="row">
                            <div class="form-group" id="para_div">
                                <input class="form-control" id="para" name="filter_para" type="date">   
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-8">
                        <div class="clearfix">&nbsp;</div>
                            <input class="btn btn-primary" type="submit" value="Filtrar">
                            <a href="{{route('alerts')}}" class="btn btn-default"> Limpar Filtro </a>
                        </div>
                    </div>
                </div>

                <!-- Fim de Divs para Filtros -->
            </form>
            

            <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row"><div class="col-sm-6"></div><div class="col-sm-6"></div></div><div class="row"><div class="col-sm-12"><table id="table_alert_user" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="table_alert_user_info">
            <thead>
                <tr role="row">
                    <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Acoes: activate to sort column descending">Ações</th>
                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending">Status</th>
                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Nome do Alerta: activate to sort column ascending">Nome do alerta</th>
                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Descrição do alerta: activate to sort column ascending">Descrição do alerta</th>
                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Data do Alerta: activate to sort column ascending">Data do Alerta</th>
                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Hora do Alerta: activate to sort column ascending">Hora do Alerta</th>
                </tr>
            </thead>
            <tbody>
                @foreach($alert_user_list as $alert_user)
                    <tr role="row" class="odd">
                        <td> 
                            
                            <a href="{{route('alert.edit').'?alertId='.$alert_user->id}}" class="btn btn-default btn-xs"> 
                                <i class="glyphicon glyphicon-edit"></i>
                            </a>
                            <!-- Implementação copiada do empresas
                            <button type="submit" class="btn btn-danger btn-xs" onclick="return confirm('Você tem certeza?')">
                                <i class="glyphicon glyphicon-trash"></i>
                            </button>
                            -->
                            <a href="{{route('alertsCad.delete').'?alertId='.$alert_user->id}}" class="btn btn-danger btn-xs" onclick="return confirm('Você tem certeza?')">
                                <i class="glyphicon glyphicon-trash"></i>
                            </a>
                        </td>
                        @if($alert_user->alertAtivo == Config::get('constants.alertAtivo_ON'))
                            <td> Ativo </td>
                        @else   
                            <td> Inativo </td>
                        @endif
                        <td> {{$alert_user->alertName}} </td>
                        <td> {{$alert_user->alertDesc}} </td>
                        <td> {{$alert_user->alertData}} </td>
                        <td> {{$alert_user->alertTime}} </td>
                    </tr>
                @endforeach

            </tbody>
        <!-- Exemplo de TFoot
            <tfoot>
            <tr><th rowspan="1" colspan="1">Rendering engine</th><th rowspan="1" colspan="1">Browser</th><th rowspan="1" colspan="1">Platform(s)</th><th rowspan="1" colspan="1">Engine version</th><th rowspan="1" colspan="1">CSS grade</th></tr>
            </tfoot>
        -->
            </table></div></div>
        <!-- Pensar em como mostrar os próximos registros depois
            <div class="row"><div class="col-sm-5"><div class="dataTables_info" id="example2_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div></div><div class="col-sm-7"><div class="dataTables_paginate paging_simple_numbers" id="example2_paginate"><ul class="pagination"><li class="paginate_button previous disabled" id="example2_previous"><a href="#" aria-controls="example2" data-dt-idx="0" tabindex="0">Previous</a></li><li class="paginate_button active"><a href="#" aria-controls="example2" data-dt-idx="1" tabindex="0">1</a></li><li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="2" tabindex="0">2</a></li><li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="3" tabindex="0">3</a></li><li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="4" tabindex="0">4</a></li><li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="5" tabindex="0">5</a></li><li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="6" tabindex="0">6</a></li><li class="paginate_button next" id="example2_next"><a href="#" aria-controls="example2" data-dt-idx="7" tabindex="0">Next</a></li></ul></div></div></div></div>
        </div>
        -->
        <!-- /.box-body -->
        </div>
@stop