@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h3>Atividade Desenvolvimento</h3>
@stop

@section('content')
    
    
    <h2>Cenário:</h2> 

    <p>
Um usuário precisa tomar decisões rápidas quando determinados eventos acontecerem no sistema, por isso ele gostaria de configurar alertas para quando esses eventos ocorrerem.

<br>
<b>Use cases:</b>
<ul>
    <li> acessa uma tela de consulta de alertas previamente cadastrados</li>
    <li> criar um novo</li>
    <li> atualizar um antigo</li>
    <li> apaga um já criado</li>
    <li> permite buscar por critérios:</li>
    <ul>
        <li> expirados</li>
        <li> ativos</li>
        <li> acionados</li>
        </ul> 
</ul>
<b>Restrições</b>

<ul>
    <li> garantir que sejam criados alertas somente se:</li>
    <ul> 
        <li> forem de tipos de eventos específicos </li>
        <li> atenderem a restrições no futuro</li>
        <li> não podem ser repetidos</li>
        </ul>
    <li> é preciso que haja testes para garantir que quaisquer lógicas de restrição, e outras,  estejam corretas</li>
</ul>    
    </p>
@stop