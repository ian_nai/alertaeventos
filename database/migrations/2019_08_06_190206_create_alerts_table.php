<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alerts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('alertName');
            $table->string('alertDesc')->nullable();
            $table->date('alertData');
            $table->boolean('alertAtivo');
            $table->time('alertTime');
            //idEvt é o Id do evento do sistema. Não há como gerar um alerta, sem que tenha um evento.
            $table->unsignedBigInteger('idEvt');
            $table->foreign('idEvt')->references('id')->on('evento_sists');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        Schema::dropIfExists('alerts');
    }
}
