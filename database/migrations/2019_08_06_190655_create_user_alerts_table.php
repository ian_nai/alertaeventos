<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAlertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alert_user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');  //cria a chave estrangeira de user alerts
                //define que a userId é estrangeira, e de qual tabela ela vem. onDelete = se remover o usuário, remove todos os userAlerts tbm
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');    
            $table->unsignedBigInteger('alert_id');
            $table->foreign('alert_id')->references('id')->on('alerts')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        //Schema::dropIfExists('user-alerts');
        //Schema::dropIfExists('user_alerts');
        Schema::dropIfExists('alert_user');
    }
}
