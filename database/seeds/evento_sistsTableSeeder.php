<?php

use Illuminate\Database\Seeder;
use app\Models\Empresa\EventoSist;

class evento_sistsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //Teste pela documentação
        DB::table('evento_sists')->insert([
            'evtName' => 'Evento 1',
            'evtDesc' => 'Descrição do evento 1',
        ]);


        DB::table('evento_sists')->insert([
            'evtName' => 'Evento 1',
            'evtDesc' => 'Descrição do evento 1',
        ]) ;

        DB::table('evento_sists')->insert([
            'evtName' => 'Evento 2',
            'evtDesc' => 'Descrição do evento 2',
        ]) ;

        DB::table('evento_sists')->insert([
            'evtName' => 'Evento 3',
            'evtDesc' => 'Descrição do evento 3',
        ]) ;

        DB::table('evento_sists')->insert([
            'evtName' => 'Evento 4',
            'evtDesc' => 'Descrição do evento 4',
        ]) ;

    }
}
