<?php

use Illuminate\Database\Seeder;
use app\Models\Empresa\Alerts;

class AlertsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('Alerts')->insert([
            'alertName' => 'Alerta Pré Criado 1',
            'alertDesc' => 'Descrição do pré criado 1',
            'alertData'  => '2019-08-12',
            'alertTime' => '08:30:00',
            'alertAtivo'=> '0',
            'IdEvt'     => '1',

        ]);

        DB::table('Alerts')->insert([
            'alertName' => 'Alerta Pré Criado 2',
            'alertDesc' => 'Descrição do pré criado 2',
            'alertData'  => '2019-08-13',
            'alertTime' => '12:00:00',
            'alertAtivo'=> '0',
            'IdEvt'     => '2',

        ]);

        DB::table('Alerts')->insert([
            'alertName' => 'Alerta Pré Criado 3',
            'alertDesc' => 'Descrição do pré criado 3',
            'alertData'  => '2019-08-18',
            'alertTime' => '02:40:00',
            'alertAtivo'=> '0',
            'IdEvt'     => '1',

        ]);
    }
}
