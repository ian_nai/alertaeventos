<?php

use Illuminate\Database\Seeder;
use App\Models\Empresa\User;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create([
            'name' => 'Usuario 1',
            'email' => 'user@user',
            'password' => bcrypt('user@user'),

        ]);

        User::create([
            'name' => 'fulano',
            'email' => 'fulano@user',
            'password' => bcrypt('fulano@user'),

        ]);
    }
}
