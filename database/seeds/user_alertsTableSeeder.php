<?php

use Illuminate\Database\Seeder;
use app\Models\Empresa\UserAlerts;

class user_alertsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('alert_user')->insert([
            'user_id'    => '1',
            'alert_id'   => '1',
        ]);

        DB::table('alert_user')->insert([
            'user_id'    => '1',
            'alert_id'   => '2',
        ]);

        DB::table('alert_user')->insert([
            'user_id'    => '1',
            'alert_id'   => '3',
        ]);

        DB::table('alert_user')->insert([
            'user_id'    => '2',
            'alert_id'   => '1',
        ]);

        DB::table('alert_user')->insert([
            'user_id'    => '1',
            'alert_id'   => '2',
        ]);

    }
}
