<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call('UsersTableSeeder');
        //User já seedado

        
        $this->call('evento_sistsTableSeeder');
        $this->call('AlertsTableSeeder');
        $this->call('user_alertsTableSeeder');

    }
}
