<?php

namespace App\Http\Requests\Business\Empresa\RegraAlertas;

use Illuminate\Foundation\Http\FormRequest;

class RegraAlertasCriacao extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'alertName' => 'required|max:100',
            'alertDesc' => 'max:255',
            'alertTime' => 'required',
            'alertData' => 'required|after_or_equal:today',
            'idEvt'     => 'required',
            'alertAtivo'=> 'required',
        ];
        
    }

    public function messages() {

        return[
            'alertName.required' => 'É necessário vincular um nome ao evento',
            'alertName.max' => 'O nome do alerta é muito grande!',
            'alertDesc.max' => 'O campo de descrição possui um texto muito grande',
            'alertData.after_or_equal' => 'A data do evento deve ser uma data futura',
            'alertData.required' => 'É necessário escolher uma data para o alerta!',
            'alertTime.required' => 'É necessário escolher um horário para o alerta!',
            'idEvt.required' => 'O alerta deve ser vinculado a um evento',
            'alertAtivo.required' => 'É necessário definir se o alerta está ativo ou inativo',
            
        ];
    }
}
