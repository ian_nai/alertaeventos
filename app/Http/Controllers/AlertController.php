<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Empresa\Alert;
use App\Models\Empresa\EventoSist;
use App\Http\Requests\Business\Empresa\RegraAlertas\RegraAlertasCriacao;
use App\Http\Requests\Business\Empresa\RegraAlertas\RegraAlertasEdicao;
use Config;

//Tentativa de pegar o currentDate
use Carbon\Carbon;

class AlertController extends Controller
{
    //
    public function alertList($status = null){
        //dd(auth()->user()->alert()->toSql());
        try{
            $alert_user_list = auth()->user()->alert;

            //por enquanto, tirando os eventos direto do BD
            if($alert_user_list == NULL){
                return view('alerts');
            } else {
                return view('alerts', compact('alert_user_list', 'status'));
            }
        }catch(Exception $e) {
            $status = ([
                'code' => Config::get('constants.Status_list_fail'),
                'msg' => Config::get('constants.Status_list_fail_msg'),
                'log' => $e,    
            ]);
            return view('alerts', compact('status'));
        }

    }

    public function filterAlert($status = null, Request $request){      
        
        try{
            $alert_user_list = NULL;
            
            switch($request->typeFilter){

                case (Config::get('constants.typeFilter_Ativos')):
                    $alert_user_list = auth()->user()->alert()->where('alertAtivo', '=', Config::get('constants.alertAtivo_ON'))->get();
                break;
                case (Config::get('constants.typeFilter_Inativos')):
                    $alert_user_list = auth()->user()->alert()->where('alertAtivo', '=', Config::get('constants.alertAtivo_OFF'))->get();
                break;
                case (Config::get('constants.typeFilter_Expirados')):
                    $today = Carbon::today();
                    $alert_user_list = auth()->user()->alert()->where('alertData', '<=', $today)->get();
                break;
                case (Config::get('constants.typeFilter_Periodo')):
                    $alert_user_list = auth()->user()->alert()->whereBetween('alertData',[$request->filter_de, $request->filter_para])->get();
                break;

            }
            return view('alerts', compact('alert_user_list','status'));
            
        }catch(Exception $e){
            $status = ([
                'code' => Config::get('constants.Status_list_fail'),
                'msg' => Config::get('constants.Status_list_fail_msg'),
                'log' => $e,    
            ]);
            return view('alerts', compact('status'));
        }
    }

    
/*  
    A função fillAlert foi dividida e melhorada nas funções editAlert e createAlert
    public function fillAlert(Request $request){
        //Busca a listagem de eventos no BD
        //$events = DB::table('evento_sists')->get();
        $events = EventoSist::all();

        //Pega o ID do alerta
        $alert_to_edit_id = $request->query('alertId');
        if($alert_to_edit_id != NULL){
            //busca o alerta já cadastrado no banco de dados
            $alert_to_edit = Alert::find($alert_to_edit_id);
            $alert = $alert_to_edit;
            
        } else {
            $alert_to_edit = NULL;
            $alert = $alert_to_edit;
        }
        return view('alertsCad', compact('events', 'alert'));
    }
*/


    //Substituindo e separando a função "fillAlert()"
    public function editAlert(Request $request){
        //Procura por todos os eventos no DB
        $events = EventoSist::all();

        $alert_id = $request->query('alertId');
        
        //Procura pelo alerta especificado somente nos alertas vinculados ao usuário
        $alert = auth()->user()->alert()->find($alert_id);//->get();//->find($alert_id)->get();
        
        if($alert == NULL){
            $status = ([
                'code' => Config::get('constants.Status_list_fail'),
                'msg' => Config::get('constants.Status_list_fail_msg'),
                'log' => '',    
            ]);
            return $this->alertList($status);
        } else {
            return view('\empresa\alerts\edicao', compact('events', 'alert'));
        }
        
    }

    //Substituindo e separando a função "fillAlert()"
    public function createAlert(){
        
        $events = EventoSist::all();

        return view('\empresa\alerts\criacao', compact('events'));
    }


    //public function insertAlert(Request $request){
    public function insertAlert(RegraAlertasCriacao $request){
        try{
            //Usando Validation
            $alert = $request->validated();
            

            //Usando Eloquent
            
            //inserir o alerta
            $alert = auth()->user()->alert()->create([
                'alertName' => $request->alertName,
                'alertDesc' => $request->alertDesc,
                'alertData' => $request->alertData,
                'alertAtivo' => $request->alertAtivo,
                'alertTime' => $request->alertTime,
                'idEvt' => $request->idEvt
            ]);

            $status = [
                'code' => Config::get('constants.Status_insert_success'),
                'msg' => Config::get('constants.Status_insert_success_msg'),
                'log' => '',    
            ];
        } catch(Exception $e) {
            $status = ([
                'code' => Config::get('constants.Status_insert_fail'),
                'msg' => Config::get('constants.Status_insert_fail_msg'),
                'log' => $e,    
            ]);
        }
        return $this->alertList($status);
    }

    //public function updateAlert(Request $request) {
    public function updateAlert(RegraAlertasEdicao $request, $id) {
        
        try{
            $alert = $request->validated();

            $alert = Alert::find($id);

            //$alert = Alert::find($alert->id);

            $alert->alertName = $request->alertName;
            $alert->alertDesc = $request->alertDesc;
            $alert->alertData = $request->alertData;
            $alert->alertAtivo = $request->alertAtivo;
            $alert->alertTime = $request->alertTime;
            $alert->idEvt = $request->idEvt;
            
            $alert->save();

            $status = [
                'code' => Config::get('constants.Status_update_success'),
                'msg' => Config::get('constants.Status_update_success_msg'),
                'log' => '',    
            ];

        } catch(Exception $e) {
            $status = [
                'code' => Config::get('constants.Status_update_fail'),
                'msg' => Config::get('constants.Status_update_fail_msg'),
                'log' => $e,    
            ];
        }

        return $this->alertList($status);
    }

    public function deleteAlert(Request $request) {

        try{
            //Verifica se o Alerta é vinculado ao usuário, antes de fazer o delete
            $alert = auth()->user()->alert()->find($request->query('alertId'));
            if($alert == NULL){
                $status = [
                    'code' => Config::get('constants.Status_delete_fail'),
                    'msg' => Config::get('constants.Status_delete_fail_msg'),
                    'log' => '',    
                ];    
            } else {
                Alert::destroy($request->query('alertId'));
                $status = [
                    'code' => Config::get('constants.Status_delete_success'),
                    'msg' => Config::get('constants.Status_delete_success_msg'),
                    'log' => '',    
                ];
            }
            
        } catch(Exception $e) {
            $status = [
                'code' => Config::get('constants.Status_delete_fail'),
                'msg' => Config::get('constants.Status_delete_fail_msg'),
                'log' => $e,    
            ];
        }

        return $this->alertList($status);
    }


    
}
