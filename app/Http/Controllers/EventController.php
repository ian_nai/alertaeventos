<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Empresa\EventoSist;

class EventController extends Controller
{
    //
    public function eventList(){
        
        $events = EventoSist::all();
        return view('events', compact('events'));
    }

    
}
