<?php

namespace App\Models\Empresa;

use Illuminate\Database\Eloquent\Model;
use app\Models\Empresa\Alert;

class EventoSist extends Model
{
    //
    protected $fillable= ['id', 'evtName', 'evtDesc' ];

    public function alerts(){
        return $this->hasMany(Alert::class);
    }
}
