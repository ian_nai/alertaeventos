<?php

namespace App\Models\Empresa;

use Illuminate\Database\Eloquent\Model;
use App\Models\Empresa\User;
use App\Models\Empresa\EventoSist;

class Alert extends Model
{
    //

    protected $fillable = ['alertName', 'alertDesc', 'alertData', 'alertTime', 'alertAtivo', 'idEvt',];

    public function users() {
        //return $this->belongsToMany(User::class, 'alert_user');
        return $this->belongsToMany(User::class);
    }

    public function event() {
        return $this->belongsTo(EventoSist::class);
    }

    public function newAlert(){}
}
