<?php

namespace App\Models\Empresa;

use Illuminate\Database\Eloquent\Model;

class UserAlert extends Model
{

    protected $fillable = ['user_id', 'alert_id'];
    //
}
